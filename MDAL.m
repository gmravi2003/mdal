function[al_struct]=...
        MDAL(trn_data,tst_data,trn_labels,...
                    tst_labels,BUDGET,LOSS_FUNCTION)
% This piece of code performs VC-UPAL but  WITH REPETITIONS...
[al_struct]=...
    PerformMDAL(trn_data,tst_data,trn_labels,tst_labels,BUDGET,LOSS_FUNCTION);
end

function[al_struct]=PerformMDAL(trn_data,tst_data,trn_labels,...
                                   tst_labels,BUDGET,LOSS_FUNCTION)

% We shall use a structure to store everything relevant
[al_struct]=CreateStructure(trn_data,tst_data,trn_labels,tst_labels,...
    BUDGET,LOSS_FUNCTION);

while(length(al_struct.queried_indices)<BUDGET)
   
    [prob_querying]=CalculateQueryProbability(al_struct);
    selected_index=randp(prob_querying,1);
    if(selected_index==0)
        display('selected index is zero!!!');
    end
    [w_tplus1]=PerformMDStep(al_struct,prob_querying,selected_index);
    % Now that we have queried a point, update the structure
    [al_struct]=UpdateStructure(al_struct,prob_querying,selected_index,w_tplus1);
    
    [trn_err_seen,trn_err_unseen,tst_err,...
        trn_err_seen_wavg,trn_err_unseen_wavg,tst_err_wavg]=...
        CalculateTrainAndTestError(al_struct);
    
    %display(tst_err);
    al_struct=UpdateErrors(al_struct,trn_err_seen,trn_err_unseen,...
        tst_err,trn_err_seen_wavg,trn_err_unseen_wavg,tst_err_wavg);
end
end

function[prob_querying]=CalculateQueryProbability(al_struct)

%p_i^t=p_min+(1-np_min)\propto |L'(y_ih_t(x_i))|
p_min=al_struct.p_min;
num_trn=al_struct.num_trn;
deriv_loss_vec=al_struct.deriv_loss_vec;
prob_querying=p_min+(1-num_trn*p_min)*abs(deriv_loss_vec)/(norm(deriv_loss_vec,1));
end


function[w_tplus1]=PerformMDStep(al_struct,prob_querying,selected_index)
%w_tplus1=
%nabRstar (\nabR(h_t) - \eta_t \sum_{i=1}^n \frac{\Qit}{\pit} \nab_h L(y_ih_t(x_i))


% Define anonymous functions to calculate gradR, and gradRstar
% First step is perform gradient descent in dual space
radius=al_struct.radius;
GRADREG=@(w)  w/(radius*(radius-norm(w)));
GRADREGSTAR=@(w) radius^2*w/(1+radius*norm(w));

p=prob_querying(selected_index);
eta=al_struct.eta_t;

y_i=al_struct.trn_labels(selected_index);
w=al_struct.w_vec;
x_i=al_struct.trn_data(:,selected_index:selected_index);
margin=y_i*w'*x_i;
dual_update_vec=GRADREG(al_struct.w_vec) - ...
    (eta/p)*al_struct.DERIVLOSS(margin)*(y_i*x_i);
w_tplus1=GRADREGSTAR(dual_update_vec);
end



% This function is called in order to update the structure after each new
% query has been made.

function[al_struct]=UpdateStructure(al_struct,prob_querying,selected_index,w_tplus1)

al_struct.w_vec=w_tplus1;
al_struct.prob_querying_mat=[al_struct.prob_querying_mat,prob_querying];

% Update wavg
al_struct.wavg=...
    (al_struct.wavg*(al_struct.iter)+al_struct.w_vec)/(al_struct.iter+1);

% Update the iteration number
al_struct.iter=al_struct.iter+1;

% Update eta. Take the old eta value and multiply by \sqrt{\frac{t}{t+1}}
al_struct.eta_t=...
    al_struct.eta_t*sqrt((al_struct.iter)/(al_struct.iter+1));

% Update queried_bool,queried_indices,and unqueried indices, 
[al_struct]=UpdateVectors(al_struct,selected_index);

% Update trn_labels_bar vector

[al_struct]=UpdateLabelBarAndLossDeriv(al_struct);
end


function[al_struct]=UpdateVectors(al_struct,selected_index)
al_struct.queried_bool_vec(selected_index)=1;
al_struct.queried_indices=find(al_struct.queried_bool_vec);
al_struct.unqueried_indices=find(~al_struct.queried_bool_vec);
end

function[al_struct]=UpdateLabelBarAndLossDeriv(al_struct)

% Update trn_labels_bar vector
new_pred=al_struct.trn_data'*al_struct.w_vec;
al_struct.trn_labels_bar(al_struct.queried_indices)=...
    al_struct.trn_labels(al_struct.queried_indices);

% Assign the label opposite to the predicted one, to unqueried indices.
al_struct.trn_labels_bar(al_struct.unqueried_indices)=...
    -sign(new_pred(al_struct.unqueried_indices));

% Update derivative vector
margin=al_struct.trn_labels_bar.*new_pred;
al_struct.deriv_loss_vec=al_struct.DERIVLOSS(margin);
end



function[al_struct]=CreateStructure(trn_data,tst_data,trn_labels,...
    tst_labels,BUDGET,LOSS_FUNCTION)

display('Going to create structure...');
al_struct=struct();
al_struct.trn_data=trn_data;
al_struct.tst_data=tst_data;
al_struct.trn_labels=trn_labels;
al_struct.tst_labels=tst_labels;
al_struct.LOSS_FUNCTION=LOSS_FUNCTION;

num_trn=size(trn_data,2);
num_tst=size(tst_data,2);
num_dims=size(trn_data,1);
al_struct.num_trn=num_trn;
al_struct.num_tst=num_tst;
al_struct.num_dims=num_dims;

al_struct.BUDGET=BUDGET;
al_struct.radius=num_trn^0.5;

% These are the quantities that need to be updated after each and 
% every round.
al_struct.w_vec=zeros(num_dims,1);
al_struct.wavg=zeros(num_dims,1);
al_struct.queried_bool_vec=zeros(num_trn,1);
al_struct.queried_indices=[];
al_struct.unqueried_indices=1:num_trn;
al_struct.trn_labels_bar=ones(num_trn,1);
% Just a counter to tell us how many iterations have been made
al_struct.iter=0;

% Intialize derivative vector of loss as per the choice of loss
% function. For hinge, exponential it is -1. For logistic it is -1/2, and for
% square it is -2

if(strcmp(al_struct.LOSS_FUNCTION,'hinge'))
   al_struct.deriv_loss_vec=-1*ones(num_trn,1); 
   al_struct.DERIVLOSS=@(p) -1*(p<=1);
   
   al_struct.p_min=1/(num_trn^2);
end

if(strcmp(al_struct.LOSS_FUNCTION,'exponential'))
   al_struct.deriv_loss_vec=-1*ones(num_trn,1); 
   al_struct.DERIVLOSS=@(p) -exp(-p);
   al_struct.p_min=0;
end


if(strcmp(al_struct.LOSS_FUNCTION,'logistic'))
  
   al_struct.deriv_loss_vec=-0.5*ones(num_trn,1); 
   al_struct.DERIVLOSS=@(p) -1./(1+exp(p));
   al_struct.p_min=0;
end

if(strcmp(al_struct.LOSS_FUNCTION,'squared'))
   al_struct.deriv_loss_vec=-2*ones(num_trn,1); 
   al_struct.DERIVLOSS=@(p) -2*(1-p);
   al_struct.p_min=0;
end


B=max(sqrt(sum(al_struct.trn_data.^2,1)));
display(al_struct.DERIVLOSS)
Gmax=abs(al_struct.DERIVLOSS(-al_struct.radius*B));
display(B);
display(Gmax);
nBGmax=al_struct.num_trn*B*Gmax;
al_struct.eta_t=(1/nBGmax)*sqrt(2*log(B*Gmax*sqrt(al_struct.num_trn)));
display('Intial eta value is');
display(al_struct.eta_t);
al_struct.prob_querying_mat=[];

% The train and test error rates
al_struct.trn_err_seen_vec=zeros(BUDGET,1);
al_struct.trn_err_unseen_vec=zeros(BUDGET,1);
al_struct.tst_err_vec=zeros(BUDGET,1);

% The train and test error rates of wavg
al_struct.trn_err_seen_wavg_vec=zeros(BUDGET,1);
al_struct.trn_err_unseen_wavg_vec=zeros(BUDGET,1);
al_struct.tst_err_wavg_vec=zeros(BUDGET,1);


end

function[trn_err_seen,trn_err_unseen,tst_err,trn_err_seen_wavg,...
    trn_err_unseen_wavg,tst_err_wavg]=...
    CalculateTrainAndTestError(al_struct)

w_vec=al_struct.w_vec;
tst_data=al_struct.tst_data;
tst_labels=al_struct.tst_labels;

trn_data=al_struct.trn_data;
trn_labels=al_struct.trn_labels;
num_tst=al_struct.num_tst;

pred_tst=sign(w_vec'*tst_data);
tst_err=sum(sign(tst_labels')~=sign(pred_tst))/num_tst;

wavg=al_struct.wavg;
pred_tst_wavg=sign(wavg'*tst_data);
tst_err_wavg=sum(sign(tst_labels')~=sign(pred_tst_wavg))/num_tst;

pred_trn=sign(w_vec'*trn_data);
pred_trn_wavg=sign(wavg'*trn_data);

queried_indices=al_struct.queried_indices;
unqueried_indices=al_struct.unqueried_indices;
num_queried_points=length(queried_indices);
num_unqueried_points=length(unqueried_indices);

trn_err_seen=sum(sign(pred_trn(queried_indices)')~=...
                 sign(trn_labels(queried_indices)))/num_queried_points;

trn_err_unseen=sum(sign(pred_trn(unqueried_indices))~=...
                   sign(trn_labels(unqueried_indices)'))/num_unqueried_points;
             
trn_err_seen_wavg=sum(sign(pred_trn_wavg(queried_indices)')~=...
                 sign(trn_labels(queried_indices)))/num_queried_points;

trn_err_unseen_wavg=sum(sign(pred_trn_wavg(unqueried_indices)')~=...
                 sign(trn_labels(unqueried_indices)))/num_unqueried_points;



end


function[al_struct]=UpdateErrors(al_struct,trn_err_seen,...
    trn_err_unseen,tst_err,trn_err_seen_wavg,trn_err_unseen_wavg,...
    tst_err_wavg)
    num_queried_points=length(al_struct.queried_indices);
    al_struct.trn_err_seen_wavg_vec(num_queried_points)=trn_err_seen_wavg;
    al_struct.trn_err_unseen_wavg_vec(num_queried_points)=trn_err_unseen_wavg;
    al_struct.tst_err_wavg_vec(num_queried_points)=tst_err_wavg;
    
    
    al_struct.trn_err_seen_vec(num_queried_points)=trn_err_seen;
    al_struct.trn_err_unseen_vec(num_queried_points)=trn_err_unseen;
    al_struct.trn_err_seen_vec(num_queried_points)=trn_err_seen;
    al_struct.tst_err_vec(num_queried_points)=tst_err;
    
end






