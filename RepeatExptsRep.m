[temp1, temp2]=system('hostname');
if(strcmp(strtrim(temp2),'leibniz'))
    path='/home/gmravi/';
else
    path='/net/hu17/gmravi/';
    N_TO_USE=8;
    LASTN=maxNumCompThreads(N_TO_USE); 
end

display('RUNNING Mirror descent BASED AL WITH REPETITIONS.....');
dirpath=strcat(path,'matlab_codes/iwal/pima/');
trn_data_file=strcat(dirpath,'pima_train_data.txt');
tst_data_file=strcat(dirpath,'pima_test_data.txt');
trn_labels_file=strcat(dirpath,'pima_train_labels.txt');
tst_labels_file=strcat(dirpath,'pima_test_labels.txt');

[trn_data,tst_data,trn_labels,tst_labels]=...
    ReadData(trn_data_file,tst_data_file,trn_labels_file,tst_labels_file);

num_trn=size(trn_data,2);
num_tst=size(tst_data,2);
trn_data=[trn_data;ones(1,num_trn)];
tst_data=[tst_data;ones(1,num_tst)];

BUDGET=300;
NUM_REPEATS=1;
tst_err_mat=zeros(BUDGET,NUM_REPEATS);
avg_of_tst_err_vec=zeros(BUDGET,1);
avg_of_trn_err_unseen_vec=zeros(BUDGET,1);
avg_of_trn_err_seen_vec=zeros(BUDGET,1);

avg_of_tst_err_wavg_vec=zeros(BUDGET,1);
avg_of_trn_err_unseen_wavg_vec=zeros(BUDGET,1);
avg_of_trn_err_seen_wavg_vec=zeros(BUDGET,1);


LOSS_FUNCTION='squared';
for repeat_num=1:NUM_REPEATS
    stream = RandStream('mt19937ar','Seed',sum(100*clock));
    [al_struct]=...
        MDAL(trn_data,tst_data,trn_labels,...
                tst_labels,BUDGET,LOSS_FUNCTION);
            
    avg_of_tst_err_vec=avg_of_tst_err_vec+al_struct.tst_err_vec;
    avg_of_trn_err_unseen_vec=...
        avg_of_trn_err_unseen_vec+al_struct.trn_err_unseen_vec;

    avg_of_trn_err_seen_vec=...
        avg_of_trn_err_seen_vec+al_struct.trn_err_seen_vec;
    tst_err_mat(:,repeat_num)=al_struct.tst_err_vec;

    % Calculations for wavg    
    avg_of_tst_err_wavg_vec=...
        avg_of_tst_err_wavg_vec+al_struct.tst_err_wavg_vec;
    
    avg_of_trn_err_unseen_wavg_vec=...
        avg_of_trn_err_unseen_wavg_vec+al_struct.trn_err_unseen_wavg_vec;

    avg_of_trn_err_seen_wavg_vec=...
        avg_of_trn_err_seen_wavg_vec+al_struct.trn_err_seen_wavg_vec;
end
avg_of_tst_err_vec=avg_of_tst_err_vec/NUM_REPEATS;
avg_of_trn_err_unseen_vec=avg_of_trn_err_unseen_vec/NUM_REPEATS;
avg_of_trn_err_seen_vec=avg_of_trn_err_seen_vec/NUM_REPEATS;
std_tst_err_vec=std(tst_err_mat');
cum_sum_avg_tst_err=sum(avg_of_tst_err_vec);
display(avg_of_tst_err_vec(end));
display(cum_sum_avg_tst_err);

% Calculation for error rates corresponding to wavg
avg_of_tst_err_wavg_vec=avg_of_tst_err_wavg_vec/NUM_REPEATS;
avg_of_trn_err_unseen_wavg_vec=avg_of_trn_err_unseen_wavg_vec/NUM_REPEATS;
avg_of_trn_err_seen_wavg_vec=avg_of_trn_err_seen_wavg_vec/NUM_REPEATS;
display('Error rates for wavg');
display('Test error of wavg at the end of the budget');
display(avg_of_tst_err_wavg_vec(end));

display('Train error unseen of wavg at the end of the BUDGET');
display(avg_of_trn_err_unseen_wavg_vec(end));

display('Train error seen of wavg at the end of the BUDGET');
display(avg_of_trn_err_seen_wavg_vec(end));
